# ansible-vaultify-py

Created in order to automate the process of Ansible-Vault encrypting variables.

## Requirements
In order for the script to execute properly, the following are required:

- path to the Ansible Vault password file, already defined in code.
- any `{VAULT_INPUT_STRING[@]}` environment variable must be defined, at least one variable must be set,
  otherwise you'll be looking at a blank screen

## Use case
Provided that a CI/CD system is deployed, create a pipeline that inputs the following environment variables:

```bash
${VAULT_INPUT_STRING[0]}
${VAULT_INPUT_STRING[1]}
${VAULT_INPUT_STRING[2]}
${VAULT_INPUT_STRING[3]}
${VAULT_INPUT_STRING[4]}
```

The script will then generate the AES 256 encrypted text that you may copy/paste it in your `./vars/main.yml` file
or wherever the encrypted variable is defined. The maximum arguments supported is set to **5**.

This is an example of valid Ansible Vault encrypted secret variable:

```yml
secret_nsa_password: !vault |
    $ANSIBLE_VAULT;1.1;AES256
    62383564626365626330336362656531316438643430666630343131666531653131333437353565
    3835313435373234366664396364366465376333623766360a646431333164363535393738663437
    35653438346431633563343330643463306166323361663836333130623964383161376336373930
    3963613130636535640a336331373932383031643036613033623930346336376465303466326239
    6433
```