# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- [LICENSE](LICENSE) for legal purposes

### Changed
- 

## [0.0.1-alpha] - 2017-06-20
### Added
- [CHANGELOG](CHANGELOG.md) to keep track of all the changes
- support for inline encryption with `ansible-vault encrypt_string`
- support for file-based encryption with `ansible-vault encrypt {file.yml}`
- print method for AES 256 resulted from the encryption operation
- cleanup leftover `*.yml` files after being encrypted and printed on the CLI


[Unreleased]: https://gitlab.com/WizDevOps/ansible-vaultify-py/compare/v1.0.0-alpha...HEAD
[0.0.1-alpha]: https://gitlab.com/WizDevOps/ansible-vaultify-py/compare/v0.0.1-alpha...v0.0.1