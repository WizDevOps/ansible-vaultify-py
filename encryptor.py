#!/usr/bin/env python3
import os
import subprocess
import sys


class AnsibleVaultEncryptor:
    """Encrypt plain-text secrets with Ansible Vault and
    print the AES 256 hash on the command line

    :param secrets_list: a list of Environment Variables that have to be encrypted
    :type secrets_list: list
    :returns: binary representation of the AES 256 hash after Ansible Vault encrypted it
    :rtype: str
    """

    def __init__(self, secrets_list):
        self.bailout_on_changes()
        self.vault_pass_file = "./vault.sh"
        self.secrets_list = list(secrets_list)
        # self.encrypt_file()
        # self.read_file
        # self.cleanup()
        self.inline_encrypt()

    def encrypt_file(self):
        """Inject the plain text in Yaml files and encrypt the files after,
        the AES 256 will replace the plain-text from Yaml files.
        """
        ansible_vault_encrypt_file = "{command} {param} {option} {value} ".format(
            command="ansible-vault",
            param="encrypt",
            option="--vault-password-file",
            value=self.vault_pass_file
        )

        for item in self.secrets_list:
            try:
                filename = "encrypt{}.yml".format(self.secrets_list.index(item) + 1)
                with open(filename, "w+") as file:
                    file.write(os.environ[item])
                    process = subprocess.Popen((ansible_vault_encrypt_file + filename).split(),
                                               stdout=subprocess.PIPE)
                    out, err = process.communicate()
                    return out, err
            except KeyError as e:
                print(e)

    @property
    def read_file(self):
        """After encrypting the file with Ansible Vault, this will print the contents of
        the encrypted .yml file."""
        for item in self.secrets_list:
            filename = "encrypt{}.yml".format(self.secrets_list.index(item) + 1)
            if os.path.exists(filename):
                with open(filename, encoding='utf-8') as file:
                    return self.output_printer(self.secrets_list.index(item) + 1, file.read())

    def cleanup(self):
        """Remove any leftovers after encrypting the files and printing them
        on the CLI. It's not critical to remove the files, they'll be overridden
        anyway on the next run, but it's best-practice."""
        for item in self.secrets_list:
            filename = "encrypt{}.yml".format(self.secrets_list.index(item) + 1)
            if os.path.exists(filename):
                os.remove(filename)

    def inline_encrypt(self):
        """Encrypt inline by using `ansible-vault encrypt_string`, it will print
        the AES 256 hash on the command line directly.

        :returns: multiple strings of the AES 256 hash
        :rtype: str
        """
        ansible_vault_encrypt_inline = "{command} {param} {option} {value} ".format(
            command="ansible-vault",
            param="encrypt_string",
            option="--vault-password-file",
            value=self.vault_pass_file
        )

        for item in self.secrets_list:
            try:
                cmd = subprocess.Popen((ansible_vault_encrypt_inline + str(os.environ[item])).split(),
                                       stdout=subprocess.PIPE)
                out, err = cmd.communicate()
                self.output_printer(self.secrets_list.index(item) + 1, out.decode())
            except KeyError as e:
                print(e)

    @staticmethod
    def output_printer(index, output):
        """Clean and print the output

        :param index: the list index of int type
        :param output: AES 256 encryption hash resulted from the encryption operation
        :return: AES 256 encryption hash of str type
        """
        print("{separator} {message} {separator}".format(
            separator="-" * 3,
            message="Secret #" + str(index)))
        print(output)

    @staticmethod
    def bailout_on_changes():
        """Stop the execution if the GoCD build is being triggered by changes,
        helps save some precious time as the GoCD Agents are already overloaded."""
        if os.environ["GO_TRIGGER_USER"] == "changes":
            print("WARNING! Pipeline triggered by changes, bailing out.")
            sys.exit(0)


def main():
    input_secrets = ['VAULT_INPUT_STRING[{}]'.format(x) for x in range(5)]
    AnsibleVaultEncryptor(input_secrets)


if __name__ == "__main__":
    main()
